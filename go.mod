require (
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/elastic/go-elasticsearch/v5 v5.6.0
	github.com/jaffee/commandeer v0.4.0
	github.com/vbauerster/mpb/v5 v5.2.4
)

module gitlab.com/piotrek.monko/blazelastic

go 1.14
