package main

import (
	"fmt"
	"github.com/jaffee/commandeer"
	"gitlab.com/piotrek.monko/blazelastic/app"
)

func main() {
	if err := commandeer.Run(app.NewApp()); err != nil {
		fmt.Println(err)
	}
}
