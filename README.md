# blazelastic

## What is it?

Blazelastic is an Elasticsearch stress-loader.

## What is it for?

Blazelastic is a tool for stress testing an Elasticsearch cluster.

## Installation

* install Go >= 1.14
* go install gitlab.com/piotrek.monko/blazelastic

## Usage

$ blazelastic -h