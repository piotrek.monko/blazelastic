package app

import (
	"log"
	"strings"
	"sync"
)

// StatsStruct collects ES response statistics
type StatsStruct struct {
	v   map[string]int
	mux sync.Mutex
}

// Inc increments given statistic metric by one
func (s *StatsStruct) Inc(key string) {
	s.mux.Lock()
	s.v[key]++
	s.mux.Unlock()
}

// Summary prints out a short summary of metrics state
func (s *StatsStruct) Summary(prefix string) {
	log.Println(strings.Repeat("-", 37))
	if len(prefix) > 0 {
		log.Println(prefix)
	}
	for k, v := range s.v {
		log.Printf("[%s] %d", k, v)
	}
}
