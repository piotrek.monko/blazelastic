package app

import (
	"encoding/json"
	"log"

	"github.com/elastic/go-elasticsearch/v5"
)

// App is the main app configuration and state management point
type App struct {
	// configuration
	Threads    int      `help:"Number of clients to simulate"`
	Docs       int      `help:"Number of documents, per client, to either write, read, or copy"`
	Host       []string `help:"Elasticsearch URL"`
	Paragraphs int      `help:"Number of paragraphs in a document"`
	IndexName  string   `help:"Target Index name"`
	Write      bool     `help:"Write documents to index"`
	Read       bool     `help:"Read documents from index"`
	Copy       string   `help:"Copy given Index into [index-name]"`
	Verbosity  int      `help:"Verbosity: 0 - show only summary, 1 - show progress and summary, 2 - 0 + 1 + show returned documents details"`
	// internals
	Es         *elasticsearch.Client `flag:"-"`
	WriteStats *StatsStruct          `flag:"-"`
	ReadStats  *StatsStruct          `flag:"-"`
}

// NewApp creates a new App instance and sets default configuration values
func NewApp() *App {
	return &App{
		Threads:    5,
		Docs:       100,
		Paragraphs: 100,
		Host:       []string{"http://localhost:9201"},
		IndexName:  "test_index_1",
		Write:      false,
		Read:       false,
		Copy:       "",
		Verbosity:  1,
		WriteStats: &StatsStruct{
			v: make(map[string]int),
		},
		ReadStats: &StatsStruct{
			v: make(map[string]int),
		},
	}
}

// Run is the main app entrypoint
func (app *App) Run() error {
	log.SetFlags(0)

	app.Es = app.Connect()

	if app.Write {
		app.WriteDocuments()
		app.WriteStats.Summary("Write Stats")
	}

	if app.Read {
		app.ReadDocuments(app.IndexName, "")
		app.ReadStats.Summary("Read Stats")
	}

	if len(app.Copy) > 0 {
		app.CopyIndex()
		app.ReadStats.Summary("Read Stats")
		app.WriteStats.Summary("Write Stats")
	}
	return nil
}

// Connect opens connection to elasticsearch server, checks if it's alive and prints server and client versions
func (app *App) Connect() *elasticsearch.Client {
	var (
		r   map[string]interface{}
		err error
	)

	escfg := elasticsearch.Config{
		Addresses: app.Host,
	}

	es, err := elasticsearch.NewClient(escfg)
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	// 1. Get cluster info
	res, err := es.Info()
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}

	// Check response status
	if res.IsError() {
		log.Fatalf("Error: %s", res.String())
	}

	// Deserialize the response into a map.
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}

	// Print client and server version numbers.
	log.Printf("Client: %s", elasticsearch.Version)
	log.Printf("Server: %s", r["version"].(map[string]interface{})["number"])

	return es
}
