package app

import (
	"log"
)

// CopyIndex copies documents from selected index into target index
func (app *App) CopyIndex() error {

	if app.Copy == app.IndexName {
		log.Fatalf("Index to copy into (%s) must be different from source index (%s)\n", app.Copy, app.IndexName)
	}

	lastScrollID := ""

	for 1 != 0 {
		docs, newScrollID, err := app.ReadDocuments(app.Copy, lastScrollID)
		if err != nil {
			log.Fatalf("Fatal failure reading docs: %s", err)
		}

		app.WriteBatch(docs)

		if len(newScrollID) != 0 {
			lastScrollID = newScrollID
		} else {
			break
		}
	}

	return nil
}
