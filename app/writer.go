package app

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	"github.com/Pallinder/go-randomdata"

	"github.com/elastic/go-elasticsearch/v5/esapi"

	"github.com/vbauerster/mpb/v5"
	"github.com/vbauerster/mpb/v5/decor"
)

// WriteBatch writes a bunch of docs into target index
func (app *App) WriteBatch(docs []interface{}) error {
	progress := mpb.New()
	bar := app.createProgressBar(progress, len(docs), len(docs), "indexing %4d documents")
	for i := range docs {
		start := time.Now()
		docID := docs[i].(map[string]interface{})["_id"]
		docBody, err := json.Marshal(docs[i].(map[string]interface{})["_source"])
		if err != nil {
			log.Fatalf("Error marshalling document: %s\n%v+", err, docs[i])
		}
		app.writeOneDocument(1, 1, docID.(string), docBody)
		if bar != nil {
			bar.Increment()
			bar.DecoratorEwmaUpdate(time.Since(start))
		}
	}
	progress.Wait()
	return nil
}

// WriteDocuments starts document generation and writes it to server
func (app *App) WriteDocuments() error {
	var wg sync.WaitGroup

	progress := mpb.New(mpb.WithWaitGroup(&wg))

	wg.Add(app.Threads)

	for i := 0; i < app.Threads; i++ {
		bar := app.createProgressBar(progress, app.Docs, i, "thread %4d -")
		go app.writerCoroutine(i, &wg, bar)
	}
	progress.Wait()

	return nil
}

func (app *App) writerCoroutine(i int, wg *sync.WaitGroup, bar *mpb.Bar) {
	defer wg.Done()

	for d := 0; d < app.Docs; d++ {
		start := time.Now()
		docID, docBody := app.generateDocument(i, d)
		app.writeOneDocument(i, d, docID, docBody)
		if bar != nil {
			bar.Increment()
			bar.DecoratorEwmaUpdate(time.Since(start))
		}
	}
}

func (app *App) generateDocument(i, d int) (string, []byte) {
	docID := fmt.Sprintf("%d-%d", d, i)
	doc := map[string]string{
		"id":    docID,
		"title": fmt.Sprintf("test %d title %d", i, d),
	}

	for p := 0; p < app.Paragraphs; p++ {
		doc[fmt.Sprintf("paragraph%d", p)] = randomdata.Paragraph()
	}

	docBody, err := json.Marshal(doc)
	if err != nil {
		log.Fatalf("Error marshalling document: %s\n%v+", err, doc)
	}
	return docID, docBody
}

func (app *App) writeOneDocument(i, d int, docID string, docBody []byte) {

	req := esapi.IndexRequest{
		Index:        app.IndexName,
		DocumentType: "test",
		Refresh:      "true",
		DocumentID:   docID,
		Body:         strings.NewReader(string(docBody)),
	}

	// Perform the request with the client.
	res, err := req.Do(context.Background(), app.Es)
	if err != nil {
		log.Printf("Error getting response: %s", err)
		return
	}
	defer res.Body.Close()

	app.WriteStats.Inc(res.Status())

	if res.IsError() {
		if app.Verbosity > 1 {
			log.Printf("[%s] Error indexing document ID=%s", res.Status(), docID)
		}
	} else {
		// Deserialize the response into a map.
		var r map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
			log.Printf("Error parsing the response body: %s", err)
		} else if res.StatusCode >= 300 {
			// Print the response status and indexed document version.
			log.Printf("[%s] %s; version=%d", res.Status(), r["result"], int(r["_version"].(float64)))
		}
	}
}

func (app *App) createProgressBar(progress *mpb.Progress, total, taskIndex int, label string) *mpb.Bar {
	if app.Verbosity == 0 {
		return nil
	}

	return progress.AddBar(
		int64(total),
		mpb.PrependDecorators(
			decor.Name(fmt.Sprintf(label, taskIndex+1)),
			decor.Percentage(decor.WCSyncSpace),
		),
		mpb.AppendDecorators(
			// replace ETA decorator with "done" message, OnComplete event
			decor.OnComplete(
				// ETA decorator with ewma age of 60
				decor.EwmaETA(decor.ET_STYLE_GO, 60), "done",
			),
		),
	)
}
