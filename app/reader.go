package app

import (
	"context"
	"encoding/json"
	"github.com/elastic/go-elasticsearch/v5/esapi"
	"log"
	"strings"
	"time"
)

// IndexedDocument stores a document contents as returned from elasticsearch server
type IndexedDocument map[string]interface{}

// ReadDocuments reads given index contents
func (app *App) ReadDocuments(fromIndex, fromScroll string) ([]interface{}, string, error) {

	var (
		r   IndexedDocument
		err error
		res *esapi.Response
	)

	// initial call must be a es.Search, only followed by es.Scroll if scrollId was returned from es.Search
	if len(fromScroll) == 0 {
		log.Printf("SEarching\n")
		res, err = app.Es.Search(
			app.Es.Search.WithContext(context.Background()),
			app.Es.Search.WithIndex(fromIndex),
			app.Es.Search.WithSort("_doc"),
			app.Es.Search.WithSize(app.Docs),
			app.Es.Search.WithScroll(time.Hour),
			app.Es.Search.WithBody(strings.NewReader(`{"query" : { "match_all" : {} }}`)),
			app.Es.Search.WithPretty(),
		)
	} else {
		log.Printf("Scanning\n")
		res, err = app.Es.Scroll(
			app.Es.Scroll.WithScrollID(fromScroll),
		)
	}
	if err != nil {
		log.Fatalf("ERROR: %s", err)
	}
	defer res.Body.Close()

	app.ReadStats.Inc(res.Status())

	if res.IsError() {
		var e IndexedDocument
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			log.Fatalf("error parsing the response body: %s", err)
		} else {
			// Print the response status and error information.
			log.Fatalf("[%s] %s: %s",
				res.Status(),
				e["error"].(map[string]interface{})["type"],
				e["error"].(map[string]interface{})["reason"],
			)
		}
	}

	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}

	// Print the ID and document source for each hit.
	log.Println(strings.Repeat("=", 37))
	if app.Verbosity > 1 {
		for _, hit := range r["hits"].(map[string]interface{})["hits"].([]map[string]interface{}) {
			log.Printf(" * ID=%s, %s", hit["_id"], hit["_source"])
		}
	}

	scrolID := ""
	if _s, found := r["_scroll_id"]; found {
		scrolID = _s.(string)
	}
	return r["hits"].(map[string]interface{})["hits"].([]interface{}), scrolID, nil
}
